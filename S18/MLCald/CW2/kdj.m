clear;
load SimulatedStock.mat

opts = detectImportOptions('Bitcoin_Data_Hourly.csv');
T = readtable('Bitcoin_Data_Hourly.csv',opts);

TMW                         = T; 
TMW_OPEN                    = table2array(TMW(:,2)); 



oscillator = stochosc(T,'NumPeriodsD',7,'NumPeriodsK',50,'Type','exponential');

zeit=1:length(TMW_OPEN);

subplot(2,1,1);
plot(T.Close)

subplot(2,1,2);
plot(zeit,oscillator.FastPercentK,zeit,oscillator.FastPercentD)
title('Stochastic Oscillator for TMW')



 clo = diff(TMW_OPEN);
 
 train_ini                  = 1;
 train_N                    = 1000; 
 
 train_data                 = TMW(train_ini: train_ini+ train_N -1,:);
 train_data_open            = TMW_OPEN(train_ini: train_ini+ train_N -1);
 train_diff                 = diff(train_data_open);
 
 train_diff                 = logical(train_diff>0);

 
 SOD = 3;
 SOK = 10;
 
 oscillator                 = stochosc(train_data,'NumPeriodsD',SOD,'NumPeriodsK',SOK,'Type','exponential');
 train_osc_K                = oscillator.FastPercentK(1:train_N-1);
 train_osc_D                = oscillator.FastPercentD(1:train_N-1);
 Gen_Data                   = [train_data_open(1:end-1),train_osc_K,train_osc_D,train_diff];
 
 Gen_Data_sel               = Gen_Data(SOD:end,:);
 B                          = glmfit(Gen_Data_sel(:,1:end-1),Gen_Data_sel(:,end),'binomial','link','logit');
 
 
 label_train                = B(1)+ Gen_Data_sel(:,1:end-1)*B(2:end);
 label_train                = logical(label_train>0);
 
 Err_train                  = train_diff(SOD:end) - label_train;
 err_rate_train             = sum(abs(Err_train))/length(Err_train);
 disp('Error_rate'),disp(err_rate_train)
 
 
 
 
 test_ini                   = 323;
 test_N                     = 600; 
 
 test_data                  = TMW(test_ini: test_ini+ test_N  -1,:);
 test_data_open             = TMW_OPEN(test_ini: test_ini+ test_N  -1);
 test_diff                  = diff(test_data_open);
 
 test_diff                  = logical(test_diff>0);
 
 
 test_oscillator            = stochosc(test_data,'NumPeriodsD',SOD,'NumPeriodsK',SOK,'Type','exponential');
 test_osc_K                 = test_oscillator.FastPercentK(1:test_N-1);
 test_osc_D                 = test_oscillator.FastPercentD(1:test_N-1);
 test_Gen_Data              = [test_data_open(1:end-1),test_osc_K,test_osc_D,test_diff];
 
 test_Gen_Data_sel          = test_Gen_Data(SOD:end,:);

 
 label_test                 = B(1)+ test_Gen_Data_sel(:,1:end-1)*B(2:end);
 label_test                 = logical(label_test>0);
 
 Err_test                   = test_diff(SOD:end) - label_test;
 err_rate_test              = sum(abs(Err_test))/length(Err_test);
 disp('Error_rate test'),disp(err_rate_test)
 