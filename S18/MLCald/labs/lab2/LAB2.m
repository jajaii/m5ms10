
clear;
close all;

TEST_ERR = [];
TRAIN_ERR = [];
TRAIN_LIKE = [];
TEST_LIKE = [];


for Polynomial_Order=1:20
    [Test_Like,Train_Like,Test_Error,Train_Error] = CompLab2_Classification(Polynomial_Order);
    TEST_ERR = [TEST_ERR Test_Error];
    TRAIN_ERR = [TRAIN_ERR Train_Error];
    TRAIN_LIKE = [TRAIN_LIKE Train_Like];
    TEST_LIKE = [TEST_LIKE Test_Like];
end

figure()
plot(1:20,TEST_ERR),hold on
plot(1:20,TRAIN_ERR),hold off
legend('test','train')
title('Test Error')
xlabel('Polynomial Order')
ylabel('Error')

figure()
plot(1:20,TEST_LIKE),hold on
plot(1:20,TRAIN_LIKE),hold off
legend('test','train')
title('Likelihood')
xlabel('Polynomial Order')
ylabel('Likelihood')


X       = load('Data_Train.txt');
Xt      = load('Data_Test.txt');
XTT     = [X;Xt];
TT       = XTT(:,3);
XTT(:,3)  = [];

ERR = [];
STD = [];

for Polynomial_Order=1:20
    [cv_err, cv_std] = CompLab1_LOOCV(XTT, TT, Polynomial_Order);
    ERR = [ERR cv_err];
    STD = [STD cv_std];
end

figure()
plot(1:20,ERR),hold on;
plot(1:20,STD),hold off;
legend('T_MSE','T_STD')


