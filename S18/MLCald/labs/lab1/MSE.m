function MSE = MSE(x,y,PolyOrder)
NumOfDataPairs = length(x);
X = ones(NumOfDataPairs, PolyOrder+1);
for i = 1:PolyOrder
    X(:,i+1) = x.^i;
end
CV = [];
for n = 1:NumOfDataPairs
   
    
    % Learn the optimal paramerers using MSE loss
    Paras_hat = (X'*X)\X'*y;
    Pred_y    = X*Paras_hat;
    
    % Calculate the MSE of prediction using training data
    CV        = [CV; (Pred_y - y).^2];
end
MSE             = mean(CV);

% N = length(x);
% Paras_hat = (x'*x)\x'*y;
% MSE = (1/N) *(y - x*Paras_hat)'*(y - x*Paras_hat);
end