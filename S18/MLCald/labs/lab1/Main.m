clear;
close all;
[x, y] = CompLab1_Create_Data();

Len = length(x);
data_element = 1:Len;
% Ones = ones(Len,1);
% Design = [Ones,x];


mse = MSE(x,y,2);



ERR = [];
STD = [];
iterset = 1:8;


for iteration = iterset
%     Training_element = randperm(70);
%     x_train = x(Training_element,:);
%     Testing_element = data_element;
%     Testing_element(Training_element) = [];
%     x_test = x(Testing_element,:);
    
    [cv_err, cv_std] = CompLab1_LOOCV(x, y, iteration);
    ERR = [ERR cv_err];
    STD = [STD cv_std];
end

figure()
plot(iterset,ERR),hold on;
plot(iterset,STD),hold off;
legend('MSE','STD')



T = readtable('long_jump_data.txt');
T = table2array(T);

T_x = T(:,1);
T_y = T(:,2);


T_ERR = [];
T_STD = [];

for iteration = iterset
%     Training_element = randperm(70);
%     x_train = x(Training_element,:);
%     Testing_element = data_element;
%     Testing_element(Training_element) = [];
%     x_test = x(Testing_element,:);
    
    [cv_err, cv_std] = CompLab1_LOOCV(T_x, T_y, iteration);
    T_ERR = [T_ERR cv_err];
    T_STD = [T_STD cv_std];
end

figure()
plot(iterset,T_ERR),hold on;
plot(iterset,T_STD),hold off;
legend('T_MSE','T_STD')


figure()
plot(T(:,1),T(:,2),'.')

