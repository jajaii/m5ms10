function [prediction,error_rate] = NBC_test_gaussian(Data_test,Pr_C,MEAN_C,cov_C1,cov_C2)
Like_test                           = Data_test(:,1:end-1);
Trait_test                          = Data_test(:,end);
N_test                              = numel(Trait_test);

THETA1 = - 0.5*(cov_C1)^-1;
THETA2 = - 0.5*(cov_C2)^-1;

theta1 = (cov_C1)^-1 * MEAN_C(1,:)';
theta2 = (cov_C2)^-1 * MEAN_C(2,:)';

b01 = 0.5*MEAN_C(1,:)*(cov_C1)^-1 * MEAN_C(1,:)' - 0.5*log(det(cov_C1)) + log(Pr_C(1));
b02 = 0.5*MEAN_C(2,:)*(cov_C2)^-1 * MEAN_C(2,:)' - 0.5*log(det(cov_C2)) + log(Pr_C(2));
g1 = dot(Like_test*THETA1,Like_test,2)' + theta1'*Like_test' + b01;
g2 = dot(Like_test*THETA2,Like_test,2)' + theta2'*Like_test' + b02;


prediction = g1/g2;
prediction(prediction>0)            = 1;
prediction(prediction<0)            = 2;
Difference                          = Trait_test - prediction;

zero_entry                          = Difference(Difference == 0);

correct_rate                        = numel(zero_entry)/N_test;
error_rate                          = 1 - correct_rate;
end