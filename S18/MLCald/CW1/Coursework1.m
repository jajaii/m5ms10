clear;
D                               = 3;
N                               = 100;
Pr_C                            = [0.5 0.5];
Pr_ij                           = [0.8,0.5;0.8,0.5;0.5,0.5];
% Pr_ij                           = [0.8,0.5;0.8,0.5;0.5,0.5]+rand(3,2)/10;

Data                            = data_generator(N,D,Pr_C,Pr_ij);           %Data Generation



train_ratio                     = 0.7;                                  
Random_index                    = randperm(N);                              %assign random selection order 
Training_index                  = Random_index(1:ceil(N*train_ratio));      %Select first train_ratio * N members' index to form the training set 
Data_train                      = Data(Training_index,:);                   %Assign training data according to selected training index
Testing_index                   = Random_index(ceil(N*train_ratio)+1:end);  %Select remaining indexes to form the testing set
Data_test                       = Data(Testing_index,:);                    %Assign testing data according to selected testing index 


%""""""""""""""""""""""""""""""""""""""""""""""""""""




[Pr_ij_train,Pr_C_train]        = NBC_train(Data_train);
[prediction,error_rate]         = NBC_test_sampling(Data_train,Pr_ij_train,Pr_C_train);



[theta,b0_constant]              = boundary_coeff(Pr_ij,Pr_C);
disp('theta ='),disp(theta)
disp('b0 ='),disp(b0_constant)

%Decision boundary plotting 
x1                              = -2:0.5:2;
[X1,X3]                         = meshgrid(x1);
X2                              = -(theta(1)/theta(2))*X1 - (theta(3)/theta(2))*X3  - b0_constant/theta(2);

X_C1_train                      = Data_train(Data_train(:,end) == 1,:);           %Data set for personality trait = 1
X_C2_train                      = Data_train(Data_train(:,end) == 2,:);           %Data set for personality trait = 2

scatter3(X_C1_train(:,1),X_C1_train(:,2),X_C1_train(:,3),'MarkerFaceColor',[0 1 1]),hold on
scatter3(X_C2_train(:,1),X_C2_train(:,2),X_C2_train(:,3),'MarkerFaceColor',[0 0.5 0.5]),hold on
surf(X1,X2,X3),hold off
xlabel('x')
ylabel('y')
zlabel('z')
legend('C1','C2')
title('Decision boundary and data points')

