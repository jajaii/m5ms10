function Data = data_generator(N,D,Pr_C,Pr_ij)

%Note: Input D is not used since information about dimension is also included in Pr_ij

Personality_1_Pr_Matrix         = repmat(Pr_C(1),N,1);                  %Create Vector of prior probability for class C1 for N subjects
Personality_trait               = binornd(1,Personality_1_Pr_Matrix)+1; %Using bernoulli distribution in generating personality traits of subjects

Like_Pr_Matrix                  = Pr_ij(:,Personality_trait');          %Generate Probability of observing 1 in feature xi with given class
Like_Matrix                     = binornd(1,Like_Pr_Matrix);            %Generate like/dislike label

Data                            = [Like_Matrix',Personality_trait];     %Combine like/dislike label with corresponding personality traits to give Data Set
end