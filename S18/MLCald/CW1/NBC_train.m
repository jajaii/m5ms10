function [Pr_ij_train,Pr_C_train]=NBC_train(Data_train)
%Seperate like/dislike data and class labels
Trait_train                     = Data_train(:,end);
Like_train                      = Data_train(:,1:end-1);

%Calculate number of subjects and number of subjects with associated label
N_train                         = numel(Trait_train);
N_C1_train                      = sum(Trait_train==1);
N_C2_train                      = sum(Trait_train==2);

%Calculate emprical prior probability of classes
Pr_C1_train                     = N_C1_train/N_train;
Pr_C2_train                     = 1 - Pr_C1_train;
Pr_C_train                      = [Pr_C1_train,Pr_C2_train];

%Isolate like/dislike data with different class 
X_C1_train                      = Like_train(Trait_train == 1,:);           %Data set for personality trait = 1
X_C2_train                      = Like_train(Trait_train == 2,:);           %Data set for personality trait = 2

%calculate empirical conditional probability
Pr_X1_C1_train                  = sum(X_C1_train,1)/N_C1_train;             %Pr(X=1|C1)
Pr_X1_C2_train                  = sum(X_C2_train,1)/N_C2_train;             %Pr(X=1|C2)
Pr_ij_train                     = [Pr_X1_C1_train',Pr_X1_C2_train'];
end