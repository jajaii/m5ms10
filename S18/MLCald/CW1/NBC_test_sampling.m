function [prediction,error_rate]=NBC_test_sampling(Data_test,Pr_ij_train,Pr_C)
%Assign prior probability
Pr_C1_train                         = Pr_C(1);                    
Pr_C2_train                         = Pr_C(2);

%Seperate like/dislike data and known class labels
Like_test                           = Data_test(:,1:end-1);
Trait_test                          = Data_test(:,end);

%Calculate number of subjects involved
N_test                              = numel(Trait_test);


%Assign conditional probability of X given C1 and C2 according to like/dislike data

%First assign every entry of like/dislike data with conditional probability x=1
%for further filtering
cond_prob_X_C1                      = Pr_ij_train(:,ones(1,N_test))';           
cond_prob_X_C2                      = Pr_ij_train(:,ones(1,N_test)*2)';

%for like/dislike entries = 1 , like_test == 0 turn them into 0 thus
%preserve absolute value of conditional probability P(xi = 1|C)
cond_prob_X_C1                      = abs((Like_test == 0)-cond_prob_X_C1);

%for like/dislike entries = 0 , like_test == 0 turn them into 1 thus
%gives  P(xi = 0|C) = 1 - P(xi = 1|C)
cond_prob_X_C2                      = abs((Like_test == 0)-cond_prob_X_C2);


%The following code is alternative equation for classifcation using the
%logged fraction
% prediction                          = log((prod(test_posterior_C1,2).*Pr_C1_train)./(prod(test_posterior_C2,2).*Pr_C2_train));
% prediction(prediction>0)            = 1;
% prediction(prediction<0)            = 2;

%Using dereived generative classifier to assign class predictions
product                             = (prod(cond_prob_X_C1,2).*Pr_C1_train)./(prod(cond_prob_X_C2,2).*Pr_C2_train);
prediction                          = product./(1+product);
prediction(prediction>0.5)          = 1;
prediction(prediction<0.5)          = 2;

%If the prediction matches known label, Difference gives 0, otherwise give
%+/- 1
Difference                          = Trait_test - prediction;
correct_prediction                  = Difference(Difference == 0);

%Calculate empirical correct rate then obtain error rate by 1 -
%correct_rate
correct_rate                        = numel(correct_prediction)/N_test;
error_rate                          = 1 - correct_rate;
end