function [theta,b0_constant] = boundary_coeff(Pr_ij,Pr_C)
log_nomi            = Pr_ij(:,1) .*(1 - Pr_ij(:,2));
log_denomi          = Pr_ij(:,2) .*(1 - Pr_ij(:,1));

theta               = log(log_nomi./log_denomi);

b0_constant         = log(Pr_C(1)/Pr_C(2)) + sum(log((1 - Pr_ij(:,1))./(1 - Pr_ij(:,2))));
end